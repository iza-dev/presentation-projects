import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header>
      <div id="logo">
        <p>
          <span>Drago</span> | 3D artist student
        </p>
      </div>
      <nav id="navigation">
        <ul>
          <li>
            <Link to="/">Projects</Link>
          </li>
          <li>
            <Link to="/about">About me</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
