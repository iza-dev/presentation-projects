import React from "react";

const Modal = ({ reveal, hide, media, typeMedia }) =>
  reveal ? (
    <>
      <div id="overlay" onClick={hide} />
      <div id="modal-container">
        {typeMedia === "video" && (
          <video autoPlay={true} muted loop>
            <source src={media} type="video/mp4" />
          </video>
        )}
        {typeMedia === "image" && <img src={media} alt="large image" />}
        <button type="button" onClick={hide}>
          <span>&times;</span>
        </button>
      </div>
    </>
  ) : null;

export default Modal;
