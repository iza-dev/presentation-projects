import React from "react";
import gif from "../assets/images/gif/avengers-infinity-war.gif";

const TextAbout = () => {
  return (
    <div id="description">
      <p>
        I'm passionate about the cyberpunk environment. I have been practicing
        3D for 5 years self-taught. Since 1 year, I have been focusing on
        Houdini which I really enjoy.
      </p>
      <img src={gif} alt="doctor strange" width="350px" />
    </div>
  );
};

export default TextAbout;
