import React, { useState } from "react";
import projects from "../data/projects";
import Modal from "./Modal";
import useModal from "../hooks/useModal";

const ListProjects = () => {
  const { openModal, toggle } = useModal();
  const [pathAsset, setPathAsset] = useState(null);
  const [typeMedia, setTypeMedia] = useState(null);

  const handleClickVideo = (element) => {
    toggle();
    setPathAsset(element.target.getAttribute("value"));
    setTypeMedia("video");
  };

  const handleClickImg = (path) => {
    toggle();
    setPathAsset(path);
    setTypeMedia("image");
  };
  return (
    <>
      <div id="list">
        <ul>
          {projects.data.map((project) => (
            <li key={project.id}>
              <div>
                <h2>{project.name}</h2>
                <img
                  src={project.images}
                  alt={project.name}
                  onClick={() => handleClickImg(project.images)}
                />
                {project.video && (
                  <p
                    id="video"
                    value={project.videos}
                    onClick={(e) => handleClickVideo(e)}
                  >
                    video
                  </p>
                )}
                <p id="softs"> {project.soft}</p>
              </div>
            </li>
          ))}
        </ul>
      </div>
      <Modal
        reveal={openModal}
        hide={toggle}
        media={pathAsset}
        typeMedia={typeMedia}
      />
    </>
  );
};

export default ListProjects;
