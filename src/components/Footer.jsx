import React from "react";

const Footer = () => {
  return (
    <footer>
      <p>Portfolio • © 2022 DRAGO</p>
    </footer>
  );
};

export default Footer;
