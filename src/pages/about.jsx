import React from "react";
import TextAbout from "../components/TextAbout";
import Header from "../components/Header";
import Footer from "../components/Footer";

const About = () => {
  return (
    <>
      <Header />
      <TextAbout />
      <Footer />
    </>
  );
};

export default About;
