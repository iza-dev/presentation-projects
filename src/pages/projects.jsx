import React from "react";
import ListProjects from "../components/ListProjects";
import Header from "../components/Header";
import Footer from "../components/Footer";

const Projects = () => {
  return (
    <>
      <Header />
      <ListProjects />
      <Footer />
    </>
  );
};

export default Projects;
