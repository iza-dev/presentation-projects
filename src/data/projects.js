import terrain1 from "../assets/images/1.jpeg";
import terrain2 from "../assets/images/2.jpeg";
import terrainUE4 from "../assets/images/3.jpeg";
import camera1 from "../assets/images/cam1.jpeg";
import camera2 from "../assets/images/camera.jpeg";
import elephant from "../assets/images/elephant.jpeg";
import ecouteur from "../assets/images/ecouteur.jpeg";
import gun1 from "../assets/images/gun1.jpeg";
import gun2 from "../assets/images/gun2.jpeg";
import gun1Fortnite from "../assets/images/ForniteGunv2.jpeg";
import gun2Fortnite from "../assets/images/ForniteGunv3.jpeg";
import axe1 from "../assets/images/AxeV1.jpeg";
import axe2 from "../assets/images/AxeV5.jpeg";
import robot from "../assets/images/RobotCartoon.jpeg";
import car from "../assets/images/car.png";
import moon from "../assets/images/moon.jpeg";
import houdini1IMG from "../assets/images/houdini-1.png";
import houdini2IMG from "../assets/images/houdini-2.png";
import houdini3IMG from "../assets/images/houdini-3.png";
import houdini4IMG from "../assets/images/houdini-4.png";
import houdini5IMG from "../assets/images/houdini-5.png";
import houdini6IMG from "../assets/images/houdini-6.png";
import sci_fi_gun4 from "../assets/images/sci_fi-gun4.jpg";
import sci_fi_gun5 from "../assets/images/sci_fi-gun5.jpg";
import houdini1 from "../assets/video/houdini-1.mp4";
import houdini2 from "../assets/video/houdini-2.mp4";
import houdini3 from "../assets/video/houdini-3.mp4";
import houdini4 from "../assets/video/houdini-4.mp4";
import houdini5 from "../assets/video/houdini-5.mp4";
import houdini6 from "../assets/video/houdini-6.mp4";
import ue4 from "../assets/video/ue4.mp4";
import girl_toon from "../assets/images/girl_toon.jpeg";
import roller_boy from "../assets/images/roller_boy.jpeg";
import uv_roller_boy from "../assets/images/uv_roller_boy.jpeg";
import fountain_0 from "../assets/images/fountain_0.jpeg";
import fountain_1 from "../assets/images/fountain_1.jpeg";
import flambleau from "../assets/images/flambeau.jpeg";
import pink_panther from "../assets/images/pink_panther.png";

const projects = {
  data: [
    {
      id: 0,
      name: "Toon Girl",
      soft: ["Blender, Evee"],
      images: girl_toon,
      video: false,
    },
    {
      id: 1,
      name: "Roller Boy",
      soft: ["ZBrush, Houdini, 3D Coat, Maya, Substance Painter"],
      images: roller_boy,
      video: false,
    },
    {
      id: 2,
      name: "Roller Boy",
      soft: ["ZBrush, Houdini, 3D Coat, Maya, Substance Painter"],
      images: uv_roller_boy,
      video: false,
    },
    {
      id: 3,
      name: "Fontaine",
      soft: ["ZBrush, Maya, Substance Painter"],
      images: fountain_0,
      video: false,
    },
    {
      id: 4,
      name: "Fontaine",
      soft: ["ZBrush, Maya, Substance Painter"],
      images: fountain_1,
      video: false,
    },
    {
      id: 5,
      name: "Flambeau",
      soft: ["ZBrush, Maya"],
      images: flambleau,
      video: false,
    },
    {
      id: 6,
      name: "Panthère Rose",
      soft: ["ZBrush"],
      images: pink_panther,
      video: false,
    },
    {
      id: 7,
      name: "Ground",
      soft: ["Houdini"],
      images: terrain1,
      video: false,
    },
    {
      id: 8,
      name: "Ground",
      soft: ["Houdini"],
      images: terrain2,
      video: false,
    },
    {
      id: 9,
      name: "GroundUE4",
      soft: ["Houdini, Engine Unreal 4"],
      images: terrainUE4,
      video: true,
      videos: ue4,
    },
    {
      id: 10,
      name: "Camera",
      soft: ["Maya"],
      images: camera1,
      video: false,
    },
    {
      id: 11,
      name: "Camera",
      soft: ["Maya"],
      images: camera2,
      video: false,
    },
    {
      id: 12,
      name: "Elephant",
      soft: ["Houdini, Nuke"],
      images: elephant,
      video: false,
    },
    {
      id: 13,
      name: "Earphone",
      soft: ["Maya"],
      images: ecouteur,
      video: false,
    },
    {
      id: 14,
      name: "Gun",
      soft: ["Maya"],
      images: gun1,
      video: false,
    },
    {
      id: 15,
      name: "Gun",
      soft: ["Maya"],
      images: gun2,
      video: false,
    },
    {
      id: 16,
      name: "Gun Fornite",
      soft: ["Maya, Substance Painter"],
      images: gun1Fortnite,
      video: false,
    },
    {
      id: 17,
      name: "Gun Fornite",
      soft: ["Maya, Substance Painter"],
      images: gun2Fortnite,
      video: false,
    },
    {
      id: 18,
      name: "Axe",
      soft: ["Maya, Zbrush, Substance Painter"],
      images: axe1,
      video: false,
    },
    {
      id: 19,
      name: "Axe",
      soft: ["Maya, Zbrush, Substance Painter"],
      images: axe2,
      video: false,
    },
    {
      id: 20,
      name: "Robot Cartoon",
      soft: ["Maya"],
      images: robot,
      video: false,
    },
    {
      id: 21,
      name: "Car",
      soft: ["Substance Painter"],
      images: car,
      video: false,
    },
    {
      id: 22,
      name: "Moon",
      soft: ["Houdini, Mantra"],
      images: moon,
      video: false,
    },
    {
      id: 23,
      name: "Houdini Training",
      soft: ["Houdini"],
      images: houdini1IMG,
      video: true,
      videos: houdini1,
    },
    {
      id: 24,
      name: "Houdini training",
      soft: ["Houdini"],
      images: houdini2IMG,
      video: true,
      videos: houdini2,
    },
    {
      id: 25,
      name: "Houdini training",
      soft: ["Houdini"],
      images: houdini3IMG,
      video: true,
      videos: houdini3,
    },
    {
      id: 26,
      name: "Houdini Training",
      soft: ["Houdini"],
      images: houdini4IMG,
      video: true,
      videos: houdini4,
    },
    {
      id: 27,
      name: "Houdini Training",
      soft: ["Houdini"],
      images: houdini5IMG,
      video: true,
      videos: houdini5,
    },
    {
      id: 28,
      name: "Houdini Training",
      soft: ["Houdini"],
      images: houdini6IMG,
      video: true,
      videos: houdini6,
    },
    {
      id: 29,
      name: "Sci-fi gun",
      soft: ["Substance Painter"],
      images: sci_fi_gun4,
      video: false,
    },
    {
      id: 30,
      name: "Sci-fi gun",
      soft: ["Substance Painter"],
      images: sci_fi_gun5,
      video: false,
    },
    
  ],
};

export default projects;
