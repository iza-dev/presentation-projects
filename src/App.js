import About from "./pages/about";
import Projects from "./pages/projects";
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div id="app">
      <Routes>
        <Route path="/" name="projects" element={<Projects />} />
        <Route path="/about" name="about" element={<About />} />
      </Routes>
    </div>
  );
}

export default App;
